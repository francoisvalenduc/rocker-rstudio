#!/bin/bash

remove_image () {
	count=$(docker images --format '{{.Repository}}:{{.Tag}}' | grep ${1} | wc -l)
	if [ $count -gt 0 ]; then
		echo Removing ${1}
        	docker rmi -f $(docker images --format '{{.Repository}}:{{.Tag}}' | grep ${1})
	fi
} 

remove_image "rstudio-custom"

docker build -t rstudio-custom  - < /usr/src/rocker-versioned2/dockerfiles/rstudio_4.2.3-custom.Dockerfile   
