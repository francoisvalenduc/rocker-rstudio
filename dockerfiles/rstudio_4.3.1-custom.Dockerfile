FROM rocker/r-ver:4.3.1

LABEL org.opencontainers.image.licenses="GPL-2.0-or-later" \
      org.opencontainers.image.source="https://github.com/rocker-org/rocker-versioned2" \
      org.opencontainers.image.vendor="Rocker Project" \
      org.opencontainers.image.authors="Carl Boettiger <cboettig@ropensci.org>"

ENV S6_VERSION=v2.1.0.2
ENV RSTUDIO_VERSION=2023.06.0+421
ENV DEFAULT_USER=rstudio
ENV PANDOC_VERSION=default
ENV QUARTO_VERSION=default

RUN /rocker_scripts/install_rstudio.sh
RUN /rocker_scripts/install_pandoc.sh
RUN /rocker_scripts/install_quarto.sh
RUN apt-get update \
    && apt-get install -y --no-install-recommends \
    libcurl4-openssl-dev libpq-dev libgdal-dev libgeos-dev libproj-dev openjdk-17-jdk libxml2-dev libsasl2-dev \
    && rm -rf /var/lib/apt/lists/* \
    && R CMD javareconf \
    && R -q -e 'install.packages(c("dplyr","shiny","curl","rvest","RSelenium","stringr","XML","lubridate","RPostgres","RPostgreSQL","leaflet","ggmap","shinyjs","shinyBS","stringr","shinythemes","DT","googleVis","xlsx","RPostgres","rjson","mongolite","data.table"))'

EXPOSE 8787

CMD ["/init"]
